package abstract_factory;

import abstract_factory.ihmiset.Ihminen;
import abstract_factory.ihmiset.JasperKoodari;

public class Main {
	public static void main(String[] args) {
		Ihminen ihminen = new JasperKoodari();
		System.out.println(ihminen);
		ihminen.eteneElämässä("opiskelija");
		System.out.println(ihminen);
		ihminen.eteneElämässä("Insinööri");
		System.out.println(ihminen);
	}
}
