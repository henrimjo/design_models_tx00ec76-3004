package abstract_factory.ihmiset;

import abstract_factory.vaatteet.Housut;
import abstract_factory.vaatteet.Kengät;
import abstract_factory.vaatteet.Paita;
import abstract_factory.vaatteet.Päähine;

public abstract class Ihminen implements Keskustelu {
	private String elämänVaihe;
	private Päähine päähine;
	private Paita paita;
	private Housut housut;
	private Kengät kengät;

	public abstract void eteneElämässä(String elämänVaihe);
	
	public void setPäähine(Päähine päähine) {
		this.päähine = päähine;
	}

	public void setPaita(Paita paita) {
		this.paita = paita;
	}

	public void setHousut(Housut housut) {
		this.housut = housut;
	}

	public void setKengät(Kengät kengät) {
		this.kengät = kengät;
	}

	public String getElämänVaihe() {
		return elämänVaihe;
	}

	public void setElämänVaihe(String elämänVaihe) {
		this.elämänVaihe = elämänVaihe;
	}

	public Päähine getPäähine() {
		return päähine;
	}

	public Paita getPaita() {
		return paita;
	}

	public Housut getHousut() {
		return housut;
	}

	public Kengät getKengät() {
		return kengät;
	}

	@Override
	public String toString() {
		return "Nimeni on " + kysyNimi() +
				"\n Olen tällä hetkellä " + kysyElämänVaihe() +
				"\n " + kysyVaatteista();
	}

	@Override
	public abstract String kysyElämänVaihe();
	public abstract String kysyNimi();
	public abstract String kysyVaatteista();
}
