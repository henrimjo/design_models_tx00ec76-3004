package abstract_factory.ihmiset;

import abstract_factory.tehtaat.VaateTehdas;
import abstract_factory.tehtaat.VaateTehdasTehdas;
import abstract_factory.vaatteet.VaateMerkki;

public class JasperKoodari extends Ihminen implements Keskustelu {

	VaateTehdas vaateTehdas;
	public JasperKoodari() {
		setElämänVaihe("tuntemattomassa elämän vaiheessa");
		vaateTehdas = VaateTehdasTehdas.valitseMerkkisi(VaateMerkki.Merkitön);
		pue("vaikka mitä hattuja", "halpoja paitoja", "monenlaisia housuja", "kuluneet tennarit");
	}
	@Override
	public String kysyElämänVaihe() {
		return getElämänVaihe();
	}

	@Override
	public String kysyNimi() {
		return "Jasper";
	}

	@Override
	public String kysyVaatteista() {
		return "Käytän " + super.getPäähine() + ", " + super.getPaita()+
				" ja " + super.getHousut() + ", " + super.getKengät();
	}
	@Override
	public void eteneElämässä(String elämänVaihe) {
		super.setElämänVaihe(elämänVaihe);
		if(elämänVaihe.toLowerCase().equals("opiskelija")) {
			vaateTehdas = VaateTehdasTehdas.valitseMerkkisi(VaateMerkki.Adidas);
			pue("Lippis", "T-paita", "Farmarit", "Lenkkarit");
		} else if(elämänVaihe.toLowerCase().equals("insinööri")) {
			vaateTehdas = VaateTehdasTehdas.valitseMerkkisi(VaateMerkki.Boss);
			pue("Lippis", "T-paita", "Farmarit", "Lenkkarit");
		}
	}
	public void pue(String päähineMalli, String paitaMalli, String housuMalli, String kenkäMalli) {
		super.setPäähine(vaateTehdas.createPäähine(päähineMalli));
		super.setPaita(vaateTehdas.createPaita(paitaMalli));
		super.setHousut(vaateTehdas.createHousut(housuMalli));
		super.setKengät(vaateTehdas.createKengät(kenkäMalli));
	}
	
}
