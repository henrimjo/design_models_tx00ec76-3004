package abstract_factory.ihmiset;

public interface Keskustelu {
	public String kysyElämänVaihe();
	public String kysyNimi();
	public String kysyVaatteista();
}
