package abstract_factory.tehtaat;

import abstract_factory.vaatteet.Päähine;
import abstract_factory.vaatteet.VaateMerkki;

public class AdidasVaateTehdas extends VaateTehdas {
	
	public AdidasVaateTehdas() {
		super(VaateMerkki.Adidas);
	}

	@Override
	public Päähine createPäähine(String malli) {
		Päähine päähine = super.createPäähine(malli);
		päähine.lisääLogo("adidas");
		return päähine;
	}
}
