package abstract_factory.tehtaat;
import abstract_factory.vaatteet.Päähine;
import abstract_factory.vaatteet.VaateMerkki;

public class BossVaateTehdas extends VaateTehdas implements VaateTilaus{

	public BossVaateTehdas() {
		super(VaateMerkki.Boss);
	}

	@Override
	public Päähine createPäähine(String malli) {
		Päähine päähine = super.createPäähine(malli);
		päähine.lisääLogo("Hugo Boss");
		return päähine;
	}
}
