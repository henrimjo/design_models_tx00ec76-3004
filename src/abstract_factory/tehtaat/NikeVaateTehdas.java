package abstract_factory.tehtaat;

import abstract_factory.vaatteet.Kengät;
import abstract_factory.vaatteet.VaateMerkki;

public class NikeVaateTehdas extends VaateTehdas implements VaateTilaus {
	
	public NikeVaateTehdas() {
		super(VaateMerkki.Nike);
	}

	@Override
	public Kengät createKengät(String malli) {
		 Kengät kengät = super.createKengät(malli);
		 kengät.lisääLogo("Nike!");
		 return kengät;
	}

}
