package abstract_factory.tehtaat;

import abstract_factory.vaatteet.Housut;
import abstract_factory.vaatteet.Kengät;
import abstract_factory.vaatteet.Paita;
import abstract_factory.vaatteet.Päähine;
import abstract_factory.vaatteet.VaateMerkki;

public class VaateTehdas{
	private VaateMerkki tehdasMerkki;
	public VaateTehdas(VaateMerkki merkki) {
		tehdasMerkki = merkki;
	}
	public Päähine createPäähine(String malli) {
		Päähine päähine = new Päähine(getTehdasMerkki());
		päähine.setMalli(malli);
		return päähine;
	}
	public Paita createPaita(String malli) {
		Paita paita = new Paita(getTehdasMerkki());
		paita.setMalli(malli);
		return paita;
	}
	public Housut createHousut(String malli) {
		Housut housut = new Housut(getTehdasMerkki());
		housut.setMalli(malli);
		return housut;
	}
	public Kengät createKengät(String malli) {
		Kengät kengät = new Kengät(getTehdasMerkki());
		kengät.setMalli(malli);
		return kengät;
	}
	
	protected VaateMerkki getTehdasMerkki() {
		return tehdasMerkki;
	}
	protected void setTehdasMerkki(VaateMerkki merkki) {
		tehdasMerkki = merkki;
	}
}
