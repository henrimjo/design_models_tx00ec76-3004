package abstract_factory.tehtaat;

import abstract_factory.vaatteet.VaateMerkki;

public class VaateTehdasTehdas {
	public static VaateTehdas valitseMerkkisi(VaateMerkki merkki) {
		if(merkki == VaateMerkki.Adidas) {
			return new AdidasVaateTehdas();
		} else if(merkki == VaateMerkki.Boss) {
			return new BossVaateTehdas();
		} else if (merkki == VaateMerkki.Nike) {
			return new NikeVaateTehdas();
		} else {
			return new VaateTehdas(merkki);
		}
	}
}
