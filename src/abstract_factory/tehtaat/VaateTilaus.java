package abstract_factory.tehtaat;

import abstract_factory.vaatteet.Vaate;

public interface VaateTilaus {
	public Vaate createPäähine(String malli);
	public Vaate createPaita(String malli);
	public Vaate createHousut(String malli);
	public Vaate createKengät(String malli);
}
