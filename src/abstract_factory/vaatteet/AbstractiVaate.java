package abstract_factory.vaatteet;

public abstract class AbstractiVaate implements Vaate{
	protected VaateMerkki merkki;
	private String malli;

	public AbstractiVaate valitseVaate() {
		return this;
	}
	public abstract void setMerkki(VaateMerkki merkki);
	public VaateMerkki getMerkki() {
		return merkki;
	}
	public abstract void lisääLogo(String merkki);
	public void setMalli(String malli) {
		this.malli = malli;
	}
	public String getMalli() {
		return malli;
	}
	
}
