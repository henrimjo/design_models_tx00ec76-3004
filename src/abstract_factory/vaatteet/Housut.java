package abstract_factory.vaatteet;

public class Housut extends AbstractiVaate implements Vaate {
	private String pehvaMerkki;
	public Housut(VaateMerkki merkki) {
		setMerkki(merkki);
	}
	@Override
	public String toString() {
		return super.getMerkki().getName() + " "+ super.getMalli();
	}

	@Override
	public void lisääLogo(String merkki) {
		pehvaMerkki = merkki;
	}

	public String getPehvaMerkki() {
		return pehvaMerkki;
	}

	@Override
	public void setMerkki(VaateMerkki merkki) {
		super.merkki = merkki;
		
	}
	
}
