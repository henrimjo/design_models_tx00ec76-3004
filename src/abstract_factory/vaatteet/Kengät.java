package abstract_factory.vaatteet;

public class Kengät extends AbstractiVaate implements Vaate {
	private String sivuLogo;
	public Kengät(VaateMerkki merkki) {
		setMerkki(merkki);
	}
	@Override
	public String toString() {
		return super.getMerkki().getName() + " "+ super.getMalli();
	}

	@Override
	public void lisääLogo(String merkki) {
		sivuLogo = merkki;
	}

	public String getSivuLogo() {
		return sivuLogo;
	}

	@Override
	public void setMerkki(VaateMerkki merkki) {
		super.merkki = merkki;
		
	}
	
}
