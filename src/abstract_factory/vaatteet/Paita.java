package abstract_factory.vaatteet;

public class Paita extends AbstractiVaate implements Vaate {
	private String paidanOlkamerkki;
	public Paita(VaateMerkki merkki) {
		setMerkki(merkki);
	}
	@Override
	public String toString() {
		return super.getMerkki().getName() + " "+ super.getMalli();
	}

	@Override
	public void lisääLogo(String merkki) {
		paidanOlkamerkki = merkki;
	}

	public String getPaidanOlkamerkki() {
		return paidanOlkamerkki;
	}

	@Override
	public void setMerkki(VaateMerkki merkki) {
		super.merkki = merkki;
		
	}
}
