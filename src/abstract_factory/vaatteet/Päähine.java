package abstract_factory.vaatteet;

public class Päähine extends AbstractiVaate implements Vaate {
	private String otsaLogo;
	public Päähine(VaateMerkki merkki) {
		setMerkki(merkki);
	}
	@Override
	public String toString() {	
		return super.merkki.getName() + " " + super.getMalli();
	}

	@Override
	public void lisääLogo(String merkki) {
		otsaLogo = merkki;
	}

	public String getOtsaLogo() {
		return otsaLogo;
	}

	@Override
	public void setMerkki(VaateMerkki merkki) {
		super.merkki = merkki;
		
	}
}
