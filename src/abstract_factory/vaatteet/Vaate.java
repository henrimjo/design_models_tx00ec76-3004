package abstract_factory.vaatteet;

public interface Vaate {
	public void setMalli(String malliNimi);
	public void setMerkki(VaateMerkki merkki);
	public VaateMerkki getMerkki();
	public abstract void lisääLogo(String merkki);
}
