package abstract_factory.vaatteet;

public enum VaateMerkki {
	Merkitön("Merkittömät"),Adidas("Adidas"), Boss("Boss"), Nike("Nike");
	
	private final String name;
	private VaateMerkki(final String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
}
