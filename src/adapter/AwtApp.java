package adapter;

import java.awt.Frame;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class AwtApp extends Frame {
	/**
	 * Frame gave this warning.
	 */
	private static final long serialVersionUID = -5957493203556086395L;

	// initializing using constructor
	public AwtApp() {

		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we){
				dispose();
			}
		});
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();
		for (int j = 0; j < gs.length; j++){
			GraphicsDevice gd = gs[j];
			try{
				this.addKeyListener(new MouseToWASD(gd));
			}catch (Exception e){
				System.err.println("for " + gd + "couldn't create robot");
			}

		}
		// frame size 300 width and 300 height
		setSize(300, 300);

		// setting the title of Frame
		setTitle("Wasd to mouse adapter.");

		// no layout manager
		setLayout(null);

		// now frame will be visible, by default it is not visible
		setVisible(true);
	}
}
