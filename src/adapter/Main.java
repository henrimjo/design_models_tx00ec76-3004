package adapter;

import java.awt.Panel;

public class Main {

	public static void main(String[] args) {
		AwtApp awtApp = new AwtApp();
		Panel panel = new Panel();
		awtApp.add(panel);
	}
}
