package adapter;

import java.awt.AWTException;
import java.awt.GraphicsDevice;
import java.awt.MouseInfo;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MouseToWASD extends Robot implements KeyListener{
	private double x, y;

	public MouseToWASD(GraphicsDevice screen) throws AWTException {
		super(screen);
	}

	private void getMouseInfo() {
		this.x = MouseInfo.getPointerInfo().getLocation().getX();
		this.y = MouseInfo.getPointerInfo().getLocation().getY();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		move(e, 5);
	}
	@Override
	public void keyPressed(KeyEvent e) {
		move(e, 5);
	}
	private void move(KeyEvent e, int str) {
		getMouseInfo();
		switch (e.getKeyCode()) {
		case 38:
			mouseMove((int) x, (int) y - str);
			break;
		case 37:
			mouseMove((int) x - str, (int) y);
			break;
		case 40:
			mouseMove((int) x, (int) y + str);
			break;
		case 39:
			mouseMove((int) x + str, (int) y);
			break;
		case 87:
			mouseMove((int) x, (int) y - str);
			break;
		case 65:
			mouseMove((int) x - str, (int) y);
			break;
		case 83:
			mouseMove((int) x, (int) y + str);
			break;
		case 68:
			mouseMove((int) x + str, (int) y);
			break;
		default:
			//Do nothing
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// Do nothing
	}

}
