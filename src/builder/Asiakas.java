package builder;

import builder.burgers.Burger;
import builder.ravintolat.Ravintola;

public class Asiakas {
	private Burger burger;
	public Asiakas(Ravintola ravintola) {
		burger = ravintola.getBurger();
	}
	public void syö() {
		burger.syö();
	}
	public void katsoBurgeria() {
		System.out.println(burger.katso());
	}
}
