package builder;

import builder.ravintolat.Hesburger;
import builder.ravintolat.McDonalds;

public class Main {

	public static void main(String[] args) {
		Asiakas mäkkäriFani = new Asiakas(new McDonalds());
		Asiakas heseAsiakas = new Asiakas(new Hesburger());
		System.out.println("Mäkkäri fani katsoo burgeriaan");
		mäkkäriFani.katsoBurgeria();
		System.out.println("Mäkkäri fani syö burgerin" + " ja katsoo sitä sen jälkeen");
		mäkkäriFani.syö();
		mäkkäriFani.katsoBurgeria();
		System.out.println("\n\n");
		System.out.println("HeseAsiakas katsoo burgeriaan");
		heseAsiakas.katsoBurgeria();
		System.out.println("HeseAsiakas syö burgerin" + " ja katsoo sitä sen jälkeen");
		heseAsiakas.syö();
		heseAsiakas.katsoBurgeria();

	}

}
