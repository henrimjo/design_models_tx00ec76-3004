package builder.burgers;

import java.util.LinkedList;

import builder.täytteet.Täyte;

public class JuustoHampurilainen implements TäyteBurger {

	LinkedList<Täyte> täytteet;

	public JuustoHampurilainen() {
		täytteet = new LinkedList<>();
	}

	@Override
	public void lisääTäyte(Täyte täyte) {
		täytteet.add(täyte);
	}

	@Override
	public Täyte poistaTäyte(Täyte täyte) {
		if (täytteet.contains(täyte)) {
			Täyte poistettava = täytteet.get(täytteet.indexOf(täyte));
			täytteet.remove(poistettava);
			return poistettava;
		}
		return null;
	}

	@Override
	public void syö() {
		täytteet.clear();

	}

	@Override
	public String katso() {
		StringBuilder koottuna = new StringBuilder();
		for (Täyte täyte : täytteet) {
			koottuna.append("|");
			koottuna.append(täyte.getNimi());
			koottuna.append("|");
		}
		if(koottuna.length() != 0) {
			return koottuna.toString();
		}
		return "sinulla ei ole hampurilaista!";
	}

}
