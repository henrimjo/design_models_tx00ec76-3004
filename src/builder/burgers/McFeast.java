package builder.burgers;

public class McFeast implements StringBurger {
	StringBuilder täyteNimet;
	public McFeast() {
		täyteNimet = new StringBuilder();
	}

	@Override
	public void lisääTäyte(String täyte) {
		täyteNimet.append("|");
		täyteNimet.append(täyte);
		täyteNimet.append("|");
	}

	@Override
	public void poistaTäyte(String täyte) {
		int index = täyteNimet.indexOf(täyte);
		if(index != -1) {
			täyteNimet.replace(index, index + täyte.length(), "");
		}
		
	}

	@Override
	public void syö() {
		täyteNimet.setLength(0);
	}

	@Override
	public String katso() {
		if(täyteNimet.toString().length() != 0) {
			return täyteNimet.toString();
		}
		return "sinulla ei ole hampurilaista!";
	}

}
