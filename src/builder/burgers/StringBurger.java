package builder.burgers;

public interface StringBurger extends Burger {
	public void lisääTäyte(String täyte);
	
	public void poistaTäyte(String täyte);
}
