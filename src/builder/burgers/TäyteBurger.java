package builder.burgers;

import builder.täytteet.Täyte;

public interface TäyteBurger extends Burger {
	public void lisääTäyte(Täyte täyte);

	public Täyte poistaTäyte(Täyte täyte);
}
