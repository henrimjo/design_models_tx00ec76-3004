package builder.ravintolat;

import builder.burgers.Burger;
import builder.burgers.JuustoHampurilainen;
import builder.burgers.TäyteBurger;
import builder.täytteet.TäyteSimppeli;

public class Hesburger implements Ravintola {

	@Override
	public Burger getBurger() {
		TäyteBurger burger = new JuustoHampurilainen();
		burger.lisääTäyte(new TäyteSimppeli("Leipä", 20));
		burger.lisääTäyte(new TäyteSimppeli("Ketsuppi", 20));
		burger.lisääTäyte(new TäyteSimppeli("Juusto", 20));
		burger.lisääTäyte(new TäyteSimppeli("Pihvi", 20));
		burger.lisääTäyte(new TäyteSimppeli("Leipä", 20));
		return burger;
	}

}
