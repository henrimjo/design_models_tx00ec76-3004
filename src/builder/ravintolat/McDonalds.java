package builder.ravintolat;

import builder.burgers.Burger;
import builder.burgers.McFeast;
import builder.burgers.StringBurger;

public class McDonalds implements Ravintola {
	@Override
	public Burger getBurger() {
		StringBurger mcBurger = new McFeast();
		mcBurger.lisääTäyte("Leipä");
		mcBurger.lisääTäyte("Tomaatti");
		mcBurger.lisääTäyte("Salaatti");
		mcBurger.lisääTäyte("majoneesi");
		mcBurger.lisääTäyte("Juusto");
		mcBurger.lisääTäyte("Pihvi");
		mcBurger.lisääTäyte("Leipä");
		return mcBurger;
	}

}
