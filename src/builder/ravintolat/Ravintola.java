package builder.ravintolat;

import builder.burgers.Burger;

public interface Ravintola {
	public Burger getBurger();
}
