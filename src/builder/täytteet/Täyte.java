package builder.täytteet;

public interface Täyte {
	public String getNimi();
	public int getWeight();
}
