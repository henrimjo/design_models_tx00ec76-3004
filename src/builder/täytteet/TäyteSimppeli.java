package builder.täytteet;

public class TäyteSimppeli implements Täyte {
	private String nimi;
	private int weight;
	public TäyteSimppeli(String nimi, int weight) {
		this.nimi = nimi;
		this.weight = weight;
	}
	
	@Override
	public String getNimi() {
		return nimi;
	}

	@Override
	public int getWeight() {
		return weight;
	}

}
