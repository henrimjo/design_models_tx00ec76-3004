package chain_of_responsibility;

public class Esimies extends PalkkaaKorottava implements PalkanKorottaja {

	public Esimies(PalkanKorottaja korottaja) {
		super(korottaja);
	}
	//korottaa kun 2% tai alle
	@Override
	public double korotaPalkkaa(double palkka, double korotusProsentti) {
		if(korotusProsentti <= 2) {
			return korota(palkka, korotusProsentti);
		}
		return palkanKorottaja.korotaPalkkaa(palkka, korotusProsentti);
	}
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
	
}
