package chain_of_responsibility;

public class Main {
	public static void main(String[] args) {
		//Hiararkian rakentaminen
		PalkanKorottaja esimies = new Esimies(new YksikönPäälikkö(new ToimitusJohtaja()));
		
		Työntekijä työntekijä = new Työntekijä(2000, esimies);
		työntekijä.pyydäKorotusta(3);
		System.out.println(työntekijä);
		työntekijä.pyydäKorotusta(2);
		System.out.println(työntekijä);
		työntekijä.pyydäKorotusta(7);
		System.out.println(työntekijä);
	}

}
