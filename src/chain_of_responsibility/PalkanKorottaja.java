package chain_of_responsibility;

public interface PalkanKorottaja {
	public double korotaPalkkaa(double palkka, double korotusProsentti);
}
