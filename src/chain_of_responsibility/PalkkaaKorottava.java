package chain_of_responsibility;

public abstract class PalkkaaKorottava {
	protected PalkanKorottaja palkanKorottaja;
	public PalkkaaKorottava(PalkanKorottaja korottaja) {
		palkanKorottaja = korottaja;
	}
	protected double korota(double palkka, double korotusProsentti) {
		System.out.println("Palkkaa suostui korottamaan: " + this.toString());
		return palkka + (palkka * (korotusProsentti / 100));
	}
	public abstract String toString();
}
