package chain_of_responsibility;

public class ToimitusJohtaja implements PalkanKorottaja{

	@Override
	public double korotaPalkkaa(double palkka, double korotusProsentti) {
		System.out.println("Palkkaa korotti Toimitusjohtaja");
		return palkka + (palkka * (korotusProsentti / 100));
	}
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
}
