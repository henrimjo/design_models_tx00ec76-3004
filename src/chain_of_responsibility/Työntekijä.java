package chain_of_responsibility;

public class Työntekijä {
	private double palkka;
	private PalkanKorottaja esimies;
	public Työntekijä(double palkka, PalkanKorottaja esimieheni) {
		this.palkka = palkka;
		esimies = esimieheni;
	}
	public void vaihdaEsimiestä(PalkanKorottaja uusiEsimies) {
		esimies = uusiEsimies;
	}
	public void pyydäKorotusta(double korotusprosentti) {
		palkka = esimies.korotaPalkkaa(palkka, korotusprosentti);
	}
	@Override
	public String toString() {
		return getClass().getSimpleName() + " jolla on palkkaa " + palkka;
	}
}
