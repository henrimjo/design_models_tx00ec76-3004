package chain_of_responsibility;

public class YksikönPäälikkö extends PalkkaaKorottava implements PalkanKorottaja{

	public YksikönPäälikkö(PalkanKorottaja korottaja) {
		super(korottaja);
	}
	//Korottaa kun 5% tai alle.
	@Override
	public double korotaPalkkaa(double palkka, double korotusProsentti) {
		if(korotusProsentti <= 5) {
			return korota(palkka, korotusProsentti);
		}
		return palkanKorottaja.korotaPalkkaa(palkka, korotusProsentti);
	}
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
	
}
