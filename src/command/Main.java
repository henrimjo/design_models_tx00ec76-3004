package command;

import command.commands.Command;
import command.commands.LiftUpCommand;
import command.commands.LowerDownCommand;

public class Main {

	public static void main(String[] args) {
		Screen screen = new Screen();
		Command lift = new LiftUpCommand(screen);
		Command lower = new LowerDownCommand(screen);
		WallButton upButton = new WallButton(lift);
		WallButton downButton = new WallButton(lower);
		//Rämpätään nappuloita
		upButton.push();
		downButton.push();
		upButton.push();
		downButton.push();
		upButton.push();
	}

}
