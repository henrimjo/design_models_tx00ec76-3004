package command;

public class Screen {
	public void liftUp() {
		System.out.println("Lifts the screen up");
	}
	public void lowerDown() {
		System.out.println("Lowers the screen down");
	}
}
