package command.commands;

import command.Screen;

public class LiftUpCommand implements Command {
	Screen sc;
	
	public LiftUpCommand(Screen sc) {
		super();
		this.sc = sc;
	}


	@Override
	public void execute() {
		sc.liftUp();
	}
	
}
