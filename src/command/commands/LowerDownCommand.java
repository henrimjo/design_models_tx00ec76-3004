package command.commands;

import command.Screen;

public class LowerDownCommand implements Command {
	Screen sc;

	public LowerDownCommand(Screen sc) {
		super();
		this.sc = sc;
	}

	@Override
	public void execute() {
		sc.lowerDown();
	}
}
