package composite;

import composite.laiteosat.Pöytätietokone;
import composite.tehtaat.TehtaanAsiakasPalvelu;
import composite.tehtaat.TietokoneTehdasTehdas;

public class Main {
    public static void main(String[] args) {
    	
    	TehtaanAsiakasPalvelu tehtaanAsiakasPalvelu = TietokoneTehdasTehdas.ValitseTietokoneLiike(2500);
    	Pöytätietokone tietokone = tehtaanAsiakasPalvelu.tilaaPöytätietokone();
    	System.out.println("Tietokoneen tiedot \n" + tietokone.toString() + " \n \n");
    	
    	tehtaanAsiakasPalvelu = TietokoneTehdasTehdas.ValitseTietokoneLiike(4500);
    	Pöytätietokone kallisTietokone = tehtaanAsiakasPalvelu.tilaaPöytätietokone();
    	System.out.println("Tietokoneen tiedot \n" + kallisTietokone.toString() + " \n \n");
    }
}
