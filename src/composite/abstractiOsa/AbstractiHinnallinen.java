package composite.abstractiOsa;

import composite.laiteInterface.ImplementsLaiteosa;
import composite.laiteInterface.Laiteosa;

public abstract class AbstractiHinnallinen implements Laiteosa {
	
	private String nimi;
	private double hinta;
	public AbstractiHinnallinen(double hinta) {
		this.hinta = hinta;
		ImplementsLaiteosa.addClassImplementing(getClass());
		setNimi("Nimetön");
	}
	
	public void setHinta(double hinta) {
		this.hinta = hinta;		
	}

	public double getHinta() {
		return hinta;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	public String getNimi() {
		return nimi;
	}
	
	@Override
	public String toString() {
		return "Laiteosan "+ getNimi() +" hinta on " + hinta;
	}
}
