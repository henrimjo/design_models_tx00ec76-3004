package composite.abstractiOsa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import composite.laiteInterface.ImplementsLaiteosa;
import composite.laiteInterface.LaiteKomposiitti;
import composite.laiteInterface.Laiteosa;

public abstract class KoosteKomponentti extends AbstractiHinnallinen implements LaiteKomposiitti {

	private double komponenttienHinnat;
	private ArrayList<Laiteosa> innerKomponentit;
	private HashMap<Class<? extends Laiteosa>, Integer> rajoiteMap;
	public KoosteKomponentti(double hinta) {
		super(hinta);
		innerKomponentit = new ArrayList<Laiteosa>();
		rajoiteMap = new HashMap<>();
		for(Class<? extends Laiteosa> luokka : ImplementsLaiteosa.getLaiteOsat()) {
			laiteRajoite(0, luokka);
		}
	}
	public void addLaiteosa(Laiteosa laiteosa) {
		if(rajoiteMap.get(laiteosa.getClass()) != null) {
			if(Collections.frequency(innerKomponentit, laiteosa) < rajoiteMap.get(laiteosa.getClass())) {
				innerKomponentit.add(laiteosa);
				setKomponentienHinnat();
			} else {
				System.err.println("Komponentti ei mahdu.");
			}
		} else {
			System.out.println("Laitteellaa ei ole määrä rajoitetta");
			innerKomponentit.add(laiteosa);
			setKomponentienHinnat();
		}
	}
	public ArrayList<Laiteosa> getLaiteOsa(Class<? extends Laiteosa> osanLuokka) {
		ArrayList<Laiteosa> subLista = new ArrayList<Laiteosa>();
		for (Laiteosa osa : innerKomponentit) {
			if(osa.getClass().equals(osanLuokka)) {
				subLista.add(osa);
			}
		}
		return subLista;
	}
	public ArrayList<LaiteKomposiitti> getKomposiitti(Class<? extends LaiteKomposiitti> komposiittiOsanLuokka) {
		ArrayList<LaiteKomposiitti> subLista = new ArrayList<>();
		for (Laiteosa osa : innerKomponentit) {
			if(osa.getClass().equals(komposiittiOsanLuokka)) {
				subLista.add((LaiteKomposiitti) osa);
			}
		}
		return subLista;
	}
	
	public void setHinta(double hinta) {
		super.setHinta(hinta);
	}
	public void setKomponentienHinnat() {
		double summa = 0;
		for(Laiteosa osa: innerKomponentit) {
			summa += osa.getHinta();
		}
		komponenttienHinnat = summa;
	}
	@Override
	public double getHinta() {
		return super.getHinta() + komponenttienHinnat;
	}
	public void laiteRajoite(int raja, Class<? extends Laiteosa> osanLuokka) {
		rajoiteMap.put(osanLuokka, raja);
	}
	@Override
	public String toString() {
		String komponentitString = super.getNimi() + " hinta on " + super.getHinta();
		for(Laiteosa osa: innerKomponentit) {
			komponentitString += "\n"+ super.getNimi() + " sisältää: " + osa.toString();
		}
		return komponentitString;
	}

}
