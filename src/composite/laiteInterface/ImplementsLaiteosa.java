package composite.laiteInterface;

import java.util.ArrayList;

public class ImplementsLaiteosa {
	private static ArrayList<Class<? extends Laiteosa>> implementaatioArrayList;
	private ImplementsLaiteosa() {
	}
	public static void addClassImplementing(Class<? extends Laiteosa> luokka) {
		if(implementaatioArrayList == null) {
			implementaatioArrayList = new ArrayList<Class<? extends Laiteosa>>();
		}
		implementaatioArrayList.add(luokka);
	}
	public static ArrayList<Class<? extends Laiteosa>> getLaiteOsat() {
		return implementaatioArrayList;
	}
}
