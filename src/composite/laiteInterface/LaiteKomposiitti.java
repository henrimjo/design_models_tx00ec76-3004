package composite.laiteInterface;

import java.util.ArrayList;

public interface LaiteKomposiitti extends Laiteosa {
	public void addLaiteosa(Laiteosa laiteosa);
	public void setHinta(double hinta);
	public void setKomponentienHinnat();
	public ArrayList<Laiteosa> getLaiteOsa(Class<? extends Laiteosa> osanLuokka);
	public ArrayList<LaiteKomposiitti> getKomposiitti(Class<? extends LaiteKomposiitti> komposiittiOsanLuokka);
	public void laiteRajoite(int raja, Class<? extends Laiteosa> osanLuokka);
}
