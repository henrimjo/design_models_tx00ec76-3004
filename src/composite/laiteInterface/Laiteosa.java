package composite.laiteInterface;

public interface Laiteosa {
	public void setNimi(String name);
	public void setHinta(double hinta);
	public double getHinta();
}
