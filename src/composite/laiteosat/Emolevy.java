package composite.laiteosat;

import composite.abstractiOsa.KoosteKomponentti;
import composite.laiteInterface.LaiteKomposiitti;

public class Emolevy extends KoosteKomponentti implements LaiteKomposiitti {
	public Emolevy(double hinta) {
		super(hinta);
		setNimi("Emolevy");
		super.laiteRajoite(1, Prosessori.class);
		super.laiteRajoite(2, Verkkokortti.class);
		super.laiteRajoite(4, Näytönohjain.class);
		super.laiteRajoite(16, Muistipiiri.class);
	}
}
