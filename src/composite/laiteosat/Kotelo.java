package composite.laiteosat;

import composite.abstractiOsa.KoosteKomponentti;
import composite.laiteInterface.LaiteKomposiitti;
import composite.laiteInterface.Laiteosa;

public class Kotelo extends KoosteKomponentti implements LaiteKomposiitti {
	public Kotelo(double hinta) {
		super(hinta);
		setNimi("Kotelo");
		super.laiteRajoite(1, Emolevy.class);
		super.laiteRajoite(1, VirtaLähde.class);
	}
	@Override
	public void addLaiteosa(Laiteosa laiteosa) {
		if(laiteosa.getClass().equals(VirtaLähde.class) ||
				laiteosa.getClass().equals(Emolevy.class)) {
			super.addLaiteosa(laiteosa);
		} else if(!super.getKomposiitti(Emolevy.class).isEmpty()) {
			super.getKomposiitti(Emolevy.class).get(0).addLaiteosa(laiteosa);
		}
	}
}
