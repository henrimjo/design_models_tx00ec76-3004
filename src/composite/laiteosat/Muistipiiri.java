package composite.laiteosat;

import composite.abstractiOsa.AbstractiHinnallinen;
import composite.laiteInterface.Laiteosa;

public class Muistipiiri extends AbstractiHinnallinen implements Laiteosa {
	public Muistipiiri(double hinta) {
		super(hinta);
		setNimi("MuistiPiiri");
	}
}
