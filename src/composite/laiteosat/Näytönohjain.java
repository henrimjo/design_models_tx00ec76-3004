package composite.laiteosat;

import composite.abstractiOsa.AbstractiHinnallinen;
import composite.laiteInterface.Laiteosa;

public class Näytönohjain extends AbstractiHinnallinen implements Laiteosa {

	public Näytönohjain(double hinta) {
		super(hinta);
		setNimi("Näytönohjain");
	}
	
}
