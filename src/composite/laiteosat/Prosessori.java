package composite.laiteosat;

import composite.abstractiOsa.AbstractiHinnallinen;
import composite.laiteInterface.Laiteosa;

public class Prosessori extends AbstractiHinnallinen implements Laiteosa {

	public Prosessori(double hinta) {
		super(hinta);
		setNimi("Prosessori");
	}

}
