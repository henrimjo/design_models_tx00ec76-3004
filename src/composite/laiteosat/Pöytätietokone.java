package composite.laiteosat;

import composite.abstractiOsa.KoosteKomponentti;
import composite.laiteInterface.LaiteKomposiitti;
import composite.laiteInterface.Laiteosa;

public class Pöytätietokone extends KoosteKomponentti implements LaiteKomposiitti {
	public Pöytätietokone(double kokoamisHinta) {
		super(kokoamisHinta);
		super.setNimi("Pöytätietokone");
		super.laiteRajoite(1, Kotelo.class);
	}
	@Override
	public void addLaiteosa(Laiteosa laiteosa) {
		if(laiteosa.getClass().equals(Kotelo.class)) {
			super.addLaiteosa(laiteosa);
		} else {
			if(super.getLaiteOsa(Kotelo.class).isEmpty()) {
				System.err.println("Kotelottomaan Pöytäkoneeseen ei voi lisätä komponentteja");
			} else {
				super.getKomposiitti(Kotelo.class).get(0).addLaiteosa(laiteosa);
			}
		}
	}
}
