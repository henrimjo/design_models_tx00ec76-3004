package composite.laiteosat;

import composite.abstractiOsa.AbstractiHinnallinen;
import composite.laiteInterface.Laiteosa;

public class Verkkokortti extends AbstractiHinnallinen implements Laiteosa {

	public Verkkokortti(double hinta) {
		super(hinta);
		setNimi("Verkkokortti");
	}

}
