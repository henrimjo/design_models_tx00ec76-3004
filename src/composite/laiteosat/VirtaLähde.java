package composite.laiteosat;

import composite.abstractiOsa.AbstractiHinnallinen;
import composite.laiteInterface.Laiteosa;

public class VirtaLähde extends AbstractiHinnallinen implements Laiteosa {

	public VirtaLähde(double hinta) {
		super(hinta);
		setNimi("VirtaLähde");
	}
}
