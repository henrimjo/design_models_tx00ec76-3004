package composite.tehtaat;

import composite.laiteosat.Pöytätietokone;

public abstract class AbstraktiTehdas implements TehtaanAsiakasPalvelu{
	
	private double hinta;
	public AbstraktiTehdas(double hinta) {
		this.hinta = hinta;
	}
	public void setHinta(double hinta) {
		this.hinta = hinta;
	}
	public double getHinta() {
		return hinta;
	}
	public abstract Pöytätietokone tilaaPöytätietokone();
	protected abstract Pöytätietokone kokoaPöytätietokone();
}
