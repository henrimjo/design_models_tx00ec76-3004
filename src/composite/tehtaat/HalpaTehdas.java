package composite.tehtaat;

import composite.laiteosat.Emolevy;
import composite.laiteosat.Kotelo;
import composite.laiteosat.Muistipiiri;
import composite.laiteosat.Näytönohjain;
import composite.laiteosat.Prosessori;
import composite.laiteosat.Pöytätietokone;
import composite.laiteosat.VirtaLähde;

public class HalpaTehdas extends AbstraktiTehdas {

	public HalpaTehdas(double hinta) {
		super(hinta);
	}

	@Override
	public Pöytätietokone tilaaPöytätietokone() {
		return kokoaPöytätietokone();
	}

	@Override
	protected Pöytätietokone kokoaPöytätietokone() {
		double hinnanOsa = super.getHinta() / 50;
		Pöytätietokone pöytätietokone = new Pöytätietokone(hinnanOsa * 3);
		pöytätietokone.addLaiteosa(new Kotelo(hinnanOsa * 5));
		pöytätietokone.addLaiteosa(new Emolevy(hinnanOsa * 10));
		pöytätietokone.addLaiteosa(new VirtaLähde(hinnanOsa * 5));
		for(int i=0;i<2;i++) {
			pöytätietokone.addLaiteosa(new Muistipiiri(hinnanOsa * 2));
		}
		pöytätietokone.addLaiteosa(new Näytönohjain(hinnanOsa * 16));
		pöytätietokone.addLaiteosa(new Prosessori(hinnanOsa * 9));
		return pöytätietokone;
	}
}
