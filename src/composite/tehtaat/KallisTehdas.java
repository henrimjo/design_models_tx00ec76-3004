package composite.tehtaat;

import composite.laiteosat.Emolevy;
import composite.laiteosat.Kotelo;
import composite.laiteosat.Muistipiiri;
import composite.laiteosat.Näytönohjain;
import composite.laiteosat.Prosessori;
import composite.laiteosat.Pöytätietokone;
import composite.laiteosat.VirtaLähde;

public class KallisTehdas extends AbstraktiTehdas {

	public KallisTehdas(double hinta) {
		super(hinta);
	}

	@Override
	public Pöytätietokone tilaaPöytätietokone() {
		return kokoaPöytätietokone();
	}

	@Override
	protected Pöytätietokone kokoaPöytätietokone() {
		double hinnanOsa = super.getHinta() / 56;
		Pöytätietokone pöytätietokone = new Pöytätietokone(hinnanOsa * 2);
		pöytätietokone.addLaiteosa(new Kotelo(hinnanOsa * 4));
		pöytätietokone.addLaiteosa(new Emolevy(hinnanOsa * 7));
		pöytätietokone.addLaiteosa(new VirtaLähde(hinnanOsa * 5));
		for(int i=0;i<4;i++) {
			pöytätietokone.addLaiteosa(new Muistipiiri(hinnanOsa * 1));
		}
		pöytätietokone.addLaiteosa(new Näytönohjain(hinnanOsa * 24));
		pöytätietokone.addLaiteosa(new Prosessori(hinnanOsa * 10));
		return pöytätietokone;
	}

}
