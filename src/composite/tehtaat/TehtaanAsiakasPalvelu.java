package composite.tehtaat;

import composite.laiteosat.Pöytätietokone;

public interface TehtaanAsiakasPalvelu {
	public Pöytätietokone tilaaPöytätietokone();
}
