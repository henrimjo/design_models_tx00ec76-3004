package composite.tehtaat;

public class TietokoneTehdasTehdas {
	public static AbstraktiTehdas ValitseTietokoneLiike(double budjetti) {
		if(budjetti < 3500 && budjetti > 300) {
			return new HalpaTehdas(budjetti);
		} else if(budjetti >= 3500) {
			return new KallisTehdas(budjetti);
		} else {
			System.err.println("Älä unelmoi Tietokoneista tuollaisella budjetilla");
			return null;
		}
	}
}
