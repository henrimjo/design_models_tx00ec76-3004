package decorator;

public interface AccessDisk {
	public void write(String message, int location);
	public String read(int location);
}
