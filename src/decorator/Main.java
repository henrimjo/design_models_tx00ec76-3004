package decorator;

import decorator.decorators.BasicEncryptor;
import decorator.decorators.DiskDecorator;
import decorator.disk.DDisk;
import decorator.disk.Disk;

public class Main {

	public static void main(String[] args) {
		DiskDecorator encryptedDisk = new BasicEncryptor();
		Disk disk = new DDisk();
		String message = "Woop Woop";
		System.out.println("Message " + message);
		disk.write(message, 0);
		System.out.println(disk.read(0));
		encryptedDisk.setDisk(disk);
		message = "Woop Woop Encryption";
		System.out.println("Message to encrypt " + message);
		encryptedDisk.write(message, 0);
		System.out.println("Message decrypted: " + encryptedDisk.read(0));
	}

}
