package decorator.decorators;

import decorator.AES;

public class BasicEncryptor extends DiskDecorator {

	private String secret;
	public BasicEncryptor() {
		AES.setKey("SomeKeyHere");
		secret = "superSecret";
	}
	public void write(String message, int location) {
		super.diskToBeEncrypted.write(AES.encrypt(message, secret), location);
	}
	public String read(int location) {
		return AES.decrypt(super.diskToBeEncrypted.read(location), secret);
	}
}
