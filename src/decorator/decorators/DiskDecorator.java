package decorator.decorators;

import decorator.AccessDisk;

public abstract class DiskDecorator implements AccessDisk {
	protected AccessDisk diskToBeEncrypted;
	
	public void setDisk(AccessDisk disk) {
		this.diskToBeEncrypted = disk;
	}
	public AccessDisk unpack() {
		diskToBeEncrypted = null;
		return diskToBeEncrypted;
	}
	
	public abstract void write(String message, int location);
	public abstract String read(int location);
}
