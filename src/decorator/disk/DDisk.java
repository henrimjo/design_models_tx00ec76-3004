package decorator.disk;

public class DDisk extends Disk {
	public DDisk() {
		super();
	}
	private String[] strings = new String[300];
	
	@Override
	public void write(String message, int location) {
		try {
			strings[location] = message;
		} catch (IndexOutOfBoundsException e) {
			System.err.println("Tuolle indexille ei voi kirjoittaa");
		}
	}
	@Override
	public String read(int location) {
		try {
			return strings[location];
		} catch (IndexOutOfBoundsException e) {
			System.err.println("Levyllä ei ole tässä sijainnissa mitään");
			return null;
		}
	}
	public String readAll() {
		String returnString = "";
		for(int i=0;i<strings.length;i++) {
			returnString += " || " + read(i);
		}
		return returnString;
	}
}
