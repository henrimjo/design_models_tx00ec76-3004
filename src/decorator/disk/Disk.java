package decorator.disk;

import decorator.AccessDisk;

public class Disk implements AccessDisk {

	String currentString;
	@Override
	public void write(String message, int location) {
		currentString = message;
	}

	@Override
	public String read(int location) {
		return currentString;
	}
	
}
