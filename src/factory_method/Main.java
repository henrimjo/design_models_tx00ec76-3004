package factory_method;

import factory_method.aterioivat.AterioivaOtus;
import factory_method.aterioivat.Ohjelmoija;
import factory_method.aterioivat.Opettaja;

public class Main {

    public static void main(String[] args) {
        AterioivaOtus opettaja = new Opettaja("Simo S");
        opettaja.aterioi();
        new Ohjelmoija("Jarkko").aterioi();
    }
    @Override
	public String toString() {
		return "FactoryMethod tehtävä";
	}
}
