package factory_method.aterioivat;

import factory_method.kulutettavat.Juoma;

public abstract class AterioivaOtus {

	private String nimi = "";
    private Juoma juoma = null;
    public Juoma getJuoma() {
		return juoma;
	}

	public void setJuoma(Juoma juoma) {
		this.juoma = juoma;
	}

	public AterioivaOtus(String nimi) {
    	this.nimi = nimi;
    }

    public abstract void haeJuoma();


    public void aterioi(){
        syö();
        juo();
    }

    public void syö(){
        System.out.println(nimi + ": Kylläpä ruoka maistuukin hyvältä");
    }

    public void juo(){
        if (juoma == null) {
            haeJuoma();
        }
        System.out.println(nimi + ": Aterian jälkeen " + juoma + " tekee terää. " + toString());
    }
    public abstract String toString();
}
