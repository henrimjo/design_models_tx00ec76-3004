package factory_method.aterioivat;

import factory_method.keittiö.JuomaTehdas;
import factory_method.kulutettavat.Juomat;

public class Ohjelmoija extends AterioivaOtus {
	
	public Ohjelmoija() {
		super("Ohjelmoija");
	}

	public Ohjelmoija(String nimi) {
		super(nimi);
	}

	@Override
	public void haeJuoma() {
		super.setJuoma(JuomaTehdas.luoJuoma(Juomat.KAHVI));
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Java goes beeps! boops!";
	}
}
