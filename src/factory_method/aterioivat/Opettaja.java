package factory_method.aterioivat;

import factory_method.keittiö.JuomaTehdas;
import factory_method.kulutettavat.Juomat;

public class Opettaja extends AterioivaOtus {

	public Opettaja() {
		super("Opettaja");
	}
    public Opettaja(String nimi) {
		super(nimi);
		// TODO Auto-generated constructor stub
	}
	@Override
	public void haeJuoma() {
		super.setJuoma(JuomaTehdas.luoJuoma(Juomat.VESI));
	}
	@Override
	public String toString() {
		return "Opettamis halujani hivelee...";
	}

}
