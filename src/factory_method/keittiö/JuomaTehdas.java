package factory_method.keittiö;

import factory_method.kulutettavat.*;


public class JuomaTehdas {

	public static Juoma luoJuoma(Juomat arvo) {
		try {
			if(arvo == Juomat.KAHVI) {
				return new Kahvi();
			} else if(arvo == Juomat.VESI) {
				return new Vesi();
			} else if(arvo == Juomat.MEHU) {
				return new Mehu();
			}
		} catch (NullPointerException e) {
			System.err.println("Miksi pyydät meiltä juomaa, jos et halua juomaa?");
		}
		return null;
	}
	
}
