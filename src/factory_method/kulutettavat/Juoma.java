package factory_method.kulutettavat;

public interface Juoma {
	public void kaatuu();
	public void juo(long milliLitraa);
	public void tayta();
}
