package factory_method.kulutettavat;

public class JuomaLasillinen implements Juoma {
	private long milliLitrat;
	private long lasinKoko;
	protected JuomaLasillinen() {
		lasinKoko = 150;
		milliLitrat = lasinKoko;
	}
	/**
	 * Juomalasista voi juoda juomaa.
	 * @param lasinKoko in milliLiters 
	 */
	protected JuomaLasillinen(long lasinKoko) {
		this.lasinKoko = lasinKoko;
		milliLitrat = lasinKoko;
	}
	@Override
	public void kaatuu() {
		milliLitrat = 0;
	}
	@Override
	public void juo(long milliLitraa) {
		if(milliLitraa <= milliLitrat) {
			milliLitrat = milliLitrat - milliLitraa;
		} else {
			System.out.println("Lasissasi ei ole riittävästi juotavaa!");
		}
		miltäJuomaMaistuu();
	}
	protected void miltäJuomaMaistuu() {
		System.out.println("Mmmm maukasta juotavaa");
	}

	@Override
	public void tayta() {
		if(milliLitrat < lasinKoko) {
			milliLitrat = lasinKoko;
		} else {
			System.out.println("Lasisi on jo täynnä!");
		}
	}
	public String toString(){
        return "juomalasi";
    }
}
