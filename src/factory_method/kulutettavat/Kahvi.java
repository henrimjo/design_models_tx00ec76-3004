package factory_method.kulutettavat;

public class Kahvi extends JuomaLasillinen {
	public Kahvi() {
		super();
	}
	public Kahvi(long lasinKoko) {
		super(lasinKoko);
	}
    @Override
    protected void miltäJuomaMaistuu() {
    	System.out.println("mmMMm kahvi on aina hyvää");
    }
	@Override
	public String toString(){
        return "kahvi";
    }
}
