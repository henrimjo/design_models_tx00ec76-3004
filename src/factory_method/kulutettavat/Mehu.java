package factory_method.kulutettavat;

public class Mehu extends JuomaLasillinen{

	public Mehu() {
		super();
	}
	public Mehu(long lasinKoko) {
		super(lasinKoko);
	}
    @Override
    protected void miltäJuomaMaistuu() {
    	System.out.println("ompas makeaa!!");
    }
	@Override
	public String toString(){
        return "mehu";
    }
}
