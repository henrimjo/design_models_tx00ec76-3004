package factory_method.kulutettavat;

public class Vesi extends JuomaLasillinen{

	public Vesi() {
		super();
	}
    public Vesi(long lasinKoko) {
		super(lasinKoko);
	}
    @Override
    protected void miltäJuomaMaistuu() {
    	System.out.println("höm... vettä");
    }
    @Override
	public String toString(){
        return "vesi";
    }
}
