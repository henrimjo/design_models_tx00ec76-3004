package flyweight.fxmain;

public class Main{}
/*
import flyweight.BorderFactory2;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage){
		try{
			Border blue = BorderFactory2.INSTANCE.getBlueBorder();
			Border red = BorderFactory2.INSTANCE.getRedBorder();
			FlowPane pane = new FlowPane();
			pane.setBorder(red);

			pane.setVgap(6);
			pane.setHgap(5);
			pane.setPrefWrapLength(2);
			Button b1 = new Button("TogglePane");
			Button b2 = new Button("ToggleBorder2");
			Button b3 = new Button("Toggle first button border");
			b1.setBorder(red);
			b2.setBorder(red);
			
			Text t = new Text("Samat kehykset nappulalla ja taustalla = " + (pane.getBorder().equals(b2.getBorder())
					+ "\nSamat kehykset nappuloilla = " + b1.getBorder().equals(b2.getBorder())));
			b1.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0){
					t.setText("Samat kehykset nappulalla ja taustalla = " + (pane.getBorder().equals(b2.getBorder())
							+ "\nSamat kehykset nappuloilla = " + b1.getBorder().equals(b2.getBorder())));
					if(pane.getBorder().equals(blue))
						pane.setBorder(red);
					else if(pane.getBorder().equals(red))
						pane.setBorder(blue);
				}
			});
			b2.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0){
					t.setText("Samat kehykset nappulalla ja taustalla = " + (pane.getBorder().equals(b2.getBorder())
							+ "\nSamat kehykset nappuloilla = " + b1.getBorder().equals(b2.getBorder())));
					if(b2.getBorder().equals(blue))
						b2.setBorder(red);
					else if(b2.getBorder().equals(red))
						b2.setBorder(blue);
				}
			});
			b3.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent arg0){
					t.setText("Samat kehykset nappulalla ja taustalla = " + (pane.getBorder().equals(b2.getBorder())
							+ "\nSamat kehykset nappuloilla = " + b1.getBorder().equals(b2.getBorder())));
					if(b1.getBorder().equals(blue))
						b1.setBorder(red);
					else if(b1.getBorder().equals(red))
						b1.setBorder(blue);
				}
			});
			pane.getChildren().add(b1);
			pane.getChildren().add(b2);
			pane.getChildren().add(t);

			Scene scene = new Scene(pane, 400, 400);
			primaryStage.setScene(scene);

			primaryStage.show();

		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public static void main(String[] args){
		launch(args);
	}
	
}
*/
