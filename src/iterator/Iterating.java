package iterator;

import java.util.Iterator;
import java.util.List;

public class Iterating extends Thread {
	private String listType;
	private Iterator<String> iterator;
	private int cycles;
	private List<String> list;

	public Iterating(List<String> list,String type, int cycles) {
		this.list = list;
		this.iterator = list.iterator();
		this.cycles = cycles;
		this.listType = type;
	}
	public Class<?> getListType() {
		return list.getClass();
	}

	public String remove(String s){
		while(iterator.hasNext()){
			try {
				String si = iterator.next();
				if(si.equals(s))
					try{
						iterator.remove();
						return s;
					}catch (Exception e){
						System.err.println("Removing failed");
					}
			}catch (Exception e) {
				System.out.println(Thread.currentThread().getName() + ": Someone removed an element cannot iterate to next");
			}
		}
		return "failed removing "+s;
	}
	private int getRandom() {
		return (int) Math.floor(Math.random() * list.size());
	}

	@Override
	public void run(){
		int i = cycles;
		while (i > 0 && iterator.hasNext()){
			System.out.println("");
			System.out.println("In "+ Thread.currentThread().getName() +
					" has iterated " + listType + " and had " + list.size() + " elements");
			System.out.println("");
			if(list.size() > 0) {
				System.out.println("In thread " + Thread.currentThread().getName() + 
						" deleting "+remove(list.get(getRandom()))+" from " + listType + " removed ");
			}
			i--;
		}
		System.out.println(Thread.currentThread() + " has stopped working. Cycles undone = " + i);
		
	}
}
