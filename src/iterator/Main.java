package iterator;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

public class Main {

	public static void main(String[] args){
		List<List<String>> listList = new ArrayList<List<String>>();
		listList.add(new ArrayList<>());
		listList.add(new LinkedList<>());
		listList.add(new Stack<>());
		listList.add(new Vector<>());
		List<Iterating> iteratings = new ArrayList<Iterating>();
		for(List<String> l : listList) {
			l.add("Cool cool");
			l.add("Web");
			l.add("GOD");
			l.add("Heheheheee!");
		}
		for (List<String> stringList : listList){
			 iteratings.add(new Iterating(stringList, stringList.getClass().getSimpleName(), 5));
			 iteratings.add(new Iterating(stringList, stringList.getClass().getSimpleName(), 5));
			 iteratings.add(new Iterating(stringList, stringList.getClass().getSimpleName(), 5));
		}
		iteratings.stream()
			.filter(i -> i.getListType().equals(ArrayList.class))
			.forEach(i -> i.start());
	}

}
