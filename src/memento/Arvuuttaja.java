package memento;

import java.util.HashSet;
import java.util.OptionalInt;
import java.util.Random;
import java.util.Set;
import java.util.stream.IntStream;

public class Arvuuttaja {
	private int maksimiArvausLuku, arvattavaLuku;
	private boolean prints;
	
	public Arvuuttaja(int maksimiArvausLuku, boolean print) {
		prints = print;
		this.maksimiArvausLuku = maksimiArvausLuku;
		arvattavaLuku = new Random().nextInt(maksimiArvausLuku-1) +1;
	}
	public void uusiArvattava(int maksimi) {
		arvattavaLuku = new Random().nextInt(maksimi-1) +1;
	}
	
	public Memento liityPeliin() {
		return new Memento(new Random().nextInt(maksimiArvausLuku-1) +1);
	}
	public boolean voitinko(MementoType mementoType) throws IllegalArgumentException {
		Memento memento = checkMementoType(mementoType);
		if(memento.getArvotut().contains(arvattavaLuku)) {
			return true;
		} else {
			return false;
		}
	}
	public void arvaa(MementoType mementoType) {
		 
		Memento memento = checkMementoType(mementoType);
		OptionalInt arvaus = IntStream
				.iterate(new Random().nextInt(maksimiArvausLuku - 1) + 1, i -> i + 1)
				.filter(i -> !memento.getArvotut().contains(i))
				.findAny();
		try {
			memento.arvaa(arvaus.getAsInt());
			if(prints) {
				System.out.println( Thread.currentThread().getName() +
						"Arvasi numeron: " + arvaus.getAsInt());
			}
		} catch (IllegalArgumentException e) {
			System.err.println("Arvaus ei kelpaa!");
		}
	}
	private Memento checkMementoType(MementoType mementoType) {
		if (mementoType instanceof Memento) {
			return (Memento) mementoType;
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	public int haeArvausMäärät(MementoType mementoType) {
		Memento memento = checkMementoType(mementoType);
		return memento.getArvotut().size();
	}
	
	private class Memento implements MementoType{
		private Set<Integer> arvatutArvot;
		private Memento(int arvo) {
			this.arvatutArvot = new HashSet<>();
			this.arvatutArvot.add(arvo);
		}
		private void arvaa(int arvottu) {
			
			arvatutArvot.add(arvottu);
		}
		private Set<Integer> getArvotut() {
			return arvatutArvot;
		}
	}
}
