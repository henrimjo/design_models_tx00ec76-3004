package memento;

public class Asiakas implements Runnable{

	Arvuuttaja arvuuttaja;
	MementoType mementoType; 
	public Asiakas(Arvuuttaja arvuuttaja) {
		this.arvuuttaja = arvuuttaja;
	}
	
	@Override
	public void run() {
		mementoType = arvuuttaja.liityPeliin();
		while (!arvuuttaja.voitinko(mementoType)) {
			arvuuttaja.arvaa(mementoType);
		}
		System.out.println(Thread.currentThread().getName()+" Arvasi oikein yrityksellä "
		+ arvuuttaja.haeArvausMäärät(mementoType) + ".");
	}
}
