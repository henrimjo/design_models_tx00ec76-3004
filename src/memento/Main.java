package memento;

public class Main {

	public static void main(String[] args) {
		Arvuuttaja arvuuttaja = new Arvuuttaja(10000000, false);
		int asiakasMäärä = 5;
		Thread[] threads = new Thread[asiakasMäärä];
		for (int i = 1; i < asiakasMäärä +1; i++) {
			Asiakas asiakas = new Asiakas(arvuuttaja);
			Thread thread = new Thread(asiakas);
			thread.setName("Thread "+ i);
			threads[i-1] = thread;
		}
		for (int i = 0; i < threads.length; i++) {
			threads[i].start();
		}
	}
}
