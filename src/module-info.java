module design_Patterns {
	requires java.base;
	requires java.xml;
	requires java.sql;
	requires java.desktop;
	requires java.management;
}