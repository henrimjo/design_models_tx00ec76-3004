package observer;

import observer.observers.AnalogClock;
import observer.observers.DigitalClock;
import observer.subjects.ClockTimer;

public class Main {

	@SuppressWarnings("deprecation") // I know about it.
	public static void main(String[] args) {
		AnalogClock analog = new AnalogClock();
		DigitalClock digital = new DigitalClock();
		ClockTimer clockTimer = new ClockTimer();
		clockTimer.addObserver(analog);
		clockTimer.addObserver(digital);
		new Thread(clockTimer).start();
	}

}
