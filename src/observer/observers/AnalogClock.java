package observer.observers;

import java.time.LocalTime;

public class AnalogClock extends Widget{

	@Override
	public String toString() {
		LocalTime time = super.getLocalTime();
		return "Analog time is: "+time.getHour()+
				":"+time.getMinute()+":"+time.getSecond();
	}

}
