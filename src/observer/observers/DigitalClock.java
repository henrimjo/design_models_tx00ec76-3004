package observer.observers;

import java.time.LocalTime;

public class DigitalClock extends Widget{

	@Override
	public String toString() {
		LocalTime time = super.getLocalTime();
		return "Digital time is: "+time.getHour()+
				":"+time.getMinute()+":"+time.getSecond();
	}

}
