package observer.observers;

import java.time.LocalTime;
import java.util.Observable;
import java.util.Observer;

@SuppressWarnings("deprecation") // I know about it.
public abstract class Widget implements Observer {
	private LocalTime localTime;
	public Widget() {
		setLocalTime(LocalTime.now());
	}
	@Override
	public void update(Observable o, Object arg) {
		try {
			this.setLocalTime((LocalTime) arg);
			System.out.println(this);
		} catch (ClassCastException e) {
			System.out.println("What the hell"
					+ ", why are you giving me bogus classes");
		}
	}
	@Override
	public abstract String toString();
	
	public LocalTime getLocalTime() {
		return localTime;
	}
	public void setLocalTime(LocalTime localTime) {
		this.localTime = localTime;
	}
}
