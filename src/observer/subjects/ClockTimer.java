package observer.subjects;

import java.time.LocalTime;
import java.util.Observable;

@SuppressWarnings("deprecation") // I know about it.
public class ClockTimer extends Observable implements Runnable {
	
	private LocalTime localTime;
	
	public ClockTimer() {
		localTime = LocalTime.now();
	}
	
	public int getHour() {
		return localTime.getHour();
	}
	public int getMinute() {
		return localTime.getMinute();
	}
	public int getSecond() {
		return localTime.getSecond();
	}
	public void run() {
		while(true) {
			//Just checking for seconds so that the clock doesn't update too fast.
			if(localTime.getSecond() != LocalTime.now().getSecond()){
				localTime = LocalTime.now();
				setChanged();
				notifyObservers(localTime);
			}
		}
	}
}
