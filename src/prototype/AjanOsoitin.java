package prototype;

public interface AjanOsoitin {
	public void asetaMaksimiAika(int limit);
	public void osoita(int aika);
	public int suunta();
}
