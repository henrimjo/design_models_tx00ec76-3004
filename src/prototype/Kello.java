package prototype;

import java.time.LocalTime;

public interface Kello extends Cloneable {
	public LocalTime getTime();
	public void stop();
	public void resume();
	public void setName(String name);
	public Kello cloneThis();
}
