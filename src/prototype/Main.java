package prototype;

public class Main {

	public static void main(String[] args) {
		Kello kello1 = new ViisariKello("Yksi");
		Kello kello2 = kello1.cloneThis();
		kello2.setName("Kaksi");
		int times = 200;
		while(times > 0) {
			times--;
			kello1.getTime();
			kello2.getTime();
			if(times == 180) {
				System.out.println("\n\n\n\n");
				kello1.stop();
				System.out.println("\n\n\n\n");
			}
			if(times == 100) {
				System.out.println("\n\n\n\n");
				kello1.resume();
				System.out.println("\n\n\n\n");
			}
			System.out.println(kello1);
			System.out.println(kello2);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
