package prototype;

public class Viisari implements AjanOsoitin {

	private final int maxAsteet = 360;
	private int osoittaa;
	private int limit;

	public Viisari(int limit) {
		this.limit = limit;
	}
	@Override
	public void asetaMaksimiAika(int limit){
		this.limit = limit;
	}
	
	@Override
	public void osoita(int aika) {
		if(aika < 1)
			osoittaa = limit;
		else if(aika > limit)
			osoittaa = 1;
		else
			osoittaa = aika;
	}	

	@Override
	public int suunta() {
		return maxAsteet / osoittaa;
	}
	@Override
	public String toString() {
		return "\t Viisari osoittaa asteelle " + osoittaa;
	}
	
	

}
