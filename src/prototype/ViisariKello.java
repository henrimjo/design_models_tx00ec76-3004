package prototype;

import java.time.LocalTime;

public class ViisariKello implements Kello {
	private LocalTime time;
	private String nimi;
	private Viisari viisari1, viisari2, viisari3;
	private boolean stopped;
	
	public ViisariKello(String nimi) {
		this.nimi = nimi;
		time = LocalTime.now();
		viisari1 = new Viisari(12);
		viisari2 = new Viisari(60);
		viisari3 = new Viisari(60);
	}
	@Override
	public LocalTime getTime() {
		if(!stopped) {
			time = LocalTime.now();
			viisari1.osoita((int) Math.ceil(time.getHour() + 1 / 2));
			viisari2.osoita(time.getMinute() + 1);
			viisari3.osoita(time.getSecond() + 1);
			return time;
		}
		return null;
	}

	@Override
	public void stop() {
		System.out.println("Kello pysähtyi");
		stopped = true;
	}

	@Override
	public void resume() {
		System.out.println("Kello käynnistyi");
		stopped = false;
	}
	@Override  
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();  
	}
	@Override
	public Kello cloneThis() {
		try {
			return (Kello) clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	} 
	@Override
	public String toString() {
		return "Kellon " +nimi+ " aika: "+time.getHour()+":"+time.getMinute()+":"+ time.getSecond() +
				" \n viisareiden tiedot: \n" + viisari1 + "\n" + viisari2 +"\n" + viisari3;
	}
	@Override
	public void setName(String name) {
		this.nimi = name;
		
	}
}
