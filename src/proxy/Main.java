package proxy;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(final String[] arguments) {
		
		Image image1 = new ProxyImage("HiRes_10MB_Photo1");
		Image image2 = new ProxyImage("HiRes_10MB_Photo2");
		Image image3 = new ProxyImage("HiRes_15MB_Photo3");
		Image image4 = new ProxyImage("LowRes_1MB_Photo1");
		Image image5 = new ProxyImage("MidRes_5MB_Photo10");
		List<Image> imageFolder = new LinkedList<Image>();
		imageFolder.add(image1);
		imageFolder.add(image2);
		imageFolder.add(image3);
		imageFolder.add(image4);
		imageFolder.add(image5);

		
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		String consoleOutput = "1";
		int iterator = 0;
		do {
			System.out.println("\n");
			System.out.println("Enter 1 to print images");
			System.out.println("Enter 2 to see image");
			System.out.println("Enter anything else to exit");
			System.out.println("\n");
			consoleOutput = scan.nextLine();
			switch (consoleOutput) {
			case "1":
				StringBuilder builder = new StringBuilder();
				for (Image image : imageFolder) {
					builder.append(image.showData());
					builder.append("\n");
				}
				System.out.println(builder);
				break;
			case "2":
				imageFolder.get(iterator).displayImage();
				if(iterator >= imageFolder.size()) {
					iterator = 0;
				} else {
					iterator++;
				}
				break;
			default:
				consoleOutput = "3";
				break;
			}
			
		} while (!consoleOutput.equals("3"));
		System.out.println("Program stopped!");
		System.exit(1);
	}

}
