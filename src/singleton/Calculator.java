package singleton;

public class Calculator {
	private double sum;
	private Logger logger = Logger.getInstance();
	public Calculator() {
		sum = 0;
	}
	public void add(double amount) {
		sum = sum + amount;
		logger.add(this.getClass().getName()+ " added by: " + amount +
				", resulted in sum of: "  + sum);
	}
	public void times(double amount) {
		sum = sum * amount;
		logger.add(this.getClass().getName() + " multiplied by: " + amount +
				", resulted in sum of: "  + sum);
	}
	public double getSum() {
		logger.add(this.getClass().getName()+ " Requested sum");
		return sum;
	}
}
