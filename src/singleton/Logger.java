package singleton;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Logger {
	private static Logger instance;
	private Map<LocalTime, String> log;
	private Logger() {
		log = new HashMap<LocalTime, String>();
	}
	public static Logger getInstance() {
		if(instance == null) {
			instance = new Logger();
		}
		return instance;
	}
	public void add(String log) {
		instance.log.put(LocalTime.now(), log);
	}
	public Set<Entry<LocalTime, String>> getAllLogs(){
		return log.entrySet();
	}
	public String getLog(LocalTime time) {
		return log.get(time);
	}
	@Override
	public String toString() {
		String outPut = "Logs collected: \n \n";
		Set<Entry<LocalTime, String>> set = Logger.getInstance().getAllLogs();
				for (Iterator<Entry<LocalTime, String>> iterator = set.iterator(); iterator.hasNext();) {
					Entry<LocalTime, String> entry = (Entry<LocalTime, String>) iterator.next();
					outPut = outPut + " \n "+ entry.getValue();
				}
		return outPut;
	}
}
