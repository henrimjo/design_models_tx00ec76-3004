package singleton;

public class Main {

	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		calculator.add(100);
		calculator.times(3);
		System.out.println("Sum is " + calculator.getSum());
		System.out.println();
		System.out.println(Logger.getInstance().getAllLogs());
	}

}
