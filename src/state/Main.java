package state;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Pokemon pokemon = new Pokemon(PokemonType.TRUBBISH);
		@SuppressWarnings("resource") // I know it is unused.
		Scanner scan = new Scanner(System.in);
		String consoleOutput = "1";
		System.out.println("This is the pokemon arena"
				+ " and we do here only one thing and one thing only"
				+ "\n Battle!");
		do {
			System.out.println("1. Make your pokemon battle!");
			System.out.println("Input anything else to leave.");
			consoleOutput = scan.nextLine();
			if(consoleOutput.equals("1")) {
				pokemon.battle();
				System.out.println(pokemon);
			}
		} while(consoleOutput.equals("1"));
		System.out.println("You left the arena!");
	}

}
