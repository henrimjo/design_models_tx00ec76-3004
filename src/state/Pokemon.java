package state;

import state.states.Evoluutio;
import state.states.TasoYksi;
/**
 * 
 * The Context class in this case is Pokemon that holds the evolution state.
 * @author Henri Johansson
 *
 */
public class Pokemon {
	private Evoluutio state;

	public Pokemon(PokemonType type) {
		state = new TasoYksi(type);
	}
	public void fight() {
		try {
			//How long the fight takes.
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(Math.random() > 0.4) {
			double xp = state.getXp() + Math.round((Math.random() + 1) * state.getXPMultiplier());
			state.setXp(xp);
			System.out.println("Your " + this + " won and gained" + xp);
		} else {
			System.out.println("Your "+ this + " lost and got no xp");
		}
	}
	public void battle() {
		for (int i = 0; i < 10; i++) {
			fight();
		}
	}
	@Override
	public String toString() {
		return state.getType(this).name().toLowerCase()+ " with xp: " + state.getXp();
	}
	public Evoluutio getState() {
		return state;
	}
	//If new state is set that is different from the previous state Xp will reset and the state will be set.
	public void setState(Evoluutio state) {
		if(state != null && state.getClass() != this.state.getClass() ) {
			this.state = state;
		}
	}
}
