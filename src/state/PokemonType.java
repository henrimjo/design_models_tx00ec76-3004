package state;

public enum PokemonType {
	GARBODOR(2),TRUBBISH(GARBODOR, 1)
	,VENUSAUR(3),IVYSAUR(VENUSAUR, 2),BULBASAUR(IVYSAUR, 1)
	,CHARIZARD(3),CHARMELEON(CHARIZARD, 2),CHARMANDER(CHARMELEON, 1);
	private final PokemonType nextType;
	private final int evolutionStep; 
	PokemonType(int step){
		this.nextType = null;
		evolutionStep = step;
	}
	PokemonType(PokemonType nextType, int step){
		this.nextType = nextType;
		evolutionStep = step;
	}
	public static PokemonType NextType(PokemonType type) {
		return type.getNextType();
	}
	public PokemonType getNextType() {
		return nextType;
	}
	public int getEvolutionStep() {
		return evolutionStep;
	}
}
