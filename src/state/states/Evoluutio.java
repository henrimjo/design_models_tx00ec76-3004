package state.states;

import state.Pokemon;
import state.PokemonType;
/**
 * The interface all the states follow.
 * @author Henri Johansson
 */
public interface Evoluutio {
	public PokemonType getType(Pokemon pokemon);
	public int getXPMultiplier();
	public void setXp(double Xp);
	public double getXp();
}
