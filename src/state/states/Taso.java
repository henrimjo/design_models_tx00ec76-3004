package state.states;

import state.Pokemon;
import state.PokemonType;
/**
 * 
 * @author Henri Johansson
 * Abstract state just because there is some functionality that would
 * otherwise repeat a lot.
 *
 */
public abstract class Taso implements Evoluutio {
	protected double xp;
	protected PokemonType type;
	public Taso(PokemonType type) {
		this.type = type;
		this.xp = 0;
	}
	protected boolean xpCheck(Pokemon pokemon, int nextLevelLimit) {
		if(xp > nextLevelLimit) {
			return true;
		}
		return false;
	}
	public void setXp(double xp) {
		this.xp = xp;
	}
	public double getXp() {
		return xp;
	}
}
