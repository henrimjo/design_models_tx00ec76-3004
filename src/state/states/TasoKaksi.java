package state.states;

import state.Pokemon;
import state.PokemonType;

public class TasoKaksi extends Taso implements Evoluutio {

	public TasoKaksi(PokemonType type) {
		super(type);
		System.out.println("Your pokemon is evolving!");
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Your pokemon evolved to " + type.name().toLowerCase());
	}
	//
	public PokemonType getType(Pokemon pokemon) {
		if(xpCheck(pokemon, 2500)) {
			if(type.getNextType() != null) {
				pokemon.setState(new TasoKolme(type.getNextType()));
			}
		}
		return type;
	}
	public int getXPMultiplier() {
		return 90;
	}
}
