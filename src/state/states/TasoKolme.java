package state.states;

import state.Pokemon;
import state.PokemonType;

public class TasoKolme extends Taso implements Evoluutio {

	public TasoKolme(PokemonType type) {
		super(type);
		System.out.println("Your pokemon is evolving!");
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Your pokemon evolved to " + type.name().toLowerCase());
	}
	//Maximum evolution, there are no steps after this.
	public PokemonType getType(Pokemon pokemon) {
		return type;
	}
	public int getXPMultiplier() {
		return 70;
	}
}
