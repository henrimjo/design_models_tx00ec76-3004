package state.states;

import state.Pokemon;
import state.PokemonType;

public class TasoYksi extends Taso implements Evoluutio {

	public TasoYksi(PokemonType type) {
		super(type);
	}

	@Override
	public PokemonType getType(Pokemon pokemon) {
		if(xpCheck(pokemon, 1200)) {
			if(type.getNextType() != null) {
				pokemon.setState(new TasoKaksi(type.getNextType()));
			}
		}
		return type;
	}
	public int getXPMultiplier() {
		return 100;
	}
	
}
