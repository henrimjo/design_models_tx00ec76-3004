package strategy;

import java.util.List;

import strategy.strategies.AfterEveryVar;
import strategy.strategies.EverySecondVar;
import strategy.strategies.EveryThirdVar;

public class ChooseConverter<T> {
	enum Operation {
		EVERY, EVERYOTHER, EVERYTHIRD
	}

	ListConverter<T> converter;

	AfterEveryVar<T> afterEvery = new AfterEveryVar<>();
	EverySecondVar<T> everySecond = new EverySecondVar<>();
	EveryThirdVar<T> everyThird = new EveryThirdVar<>();

	public String listToString(List<T> list, Operation operation) {
		switch (operation) {
		case EVERY:
			converter = afterEvery;
			break;
		case EVERYOTHER:
			converter = everySecond;
			break;
		case EVERYTHIRD:
			converter = everyThird;
			break;
		default:
			converter = new EveryThirdVar<>();
			break;
		}
		return converter.listToString(list);
	}

}
