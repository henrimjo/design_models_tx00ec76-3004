package strategy;

import java.util.List;

public interface ListConverter<T> {
	public String listToString(List<T> list);
}
