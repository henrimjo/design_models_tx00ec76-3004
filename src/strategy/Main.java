package strategy;

import java.util.List;
import java.util.stream.Stream;

import strategy.ChooseConverter.Operation;

public class Main {

	public static void main(String[] args) {
		List<Character> charList = Stream.of('A', 'B', 'C', 'D').toList();
		ChooseConverter<Character> converter = new ChooseConverter<>();
		System.out.println("For every char: " + converter.listToString(charList, Operation.EVERY));
		System.out.println("For every other char: " + converter.listToString(charList, Operation.EVERYOTHER));
		System.out.println("For every third char: " + converter.listToString(charList, Operation.EVERYTHIRD));
		
	}

}
