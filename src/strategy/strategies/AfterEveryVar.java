package strategy.strategies;

import java.util.List;
import java.util.stream.Collectors;

import strategy.ListConverter;

public class AfterEveryVar<T> implements ListConverter<T>{

	@Override
	public String listToString(List<T> list) {
		return list.stream()
			      .map(n -> String.valueOf(n))
			      .collect(Collectors.joining("\n", "" , ""));
	}
	
}
