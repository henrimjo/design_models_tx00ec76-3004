package strategy.strategies;

import java.util.List;

import strategy.ListConverter;

public class EveryThirdVar<T> implements ListConverter<T> {

	@Override
	public String listToString(List<T> list) {
		String returnS = "";
		for (int i = 0; i < list.size(); i++) {
			if ((i) % 3 == 0) {
				returnS += "\n";
			}
			returnS += list.get(i).toString();
		}
		return returnS;

	}

}
