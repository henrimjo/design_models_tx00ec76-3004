package template_method;

import template_method.game.Game;
import template_method.game.RpgFighter;

public class Main {

	public static void main(String[] args) {
		Game game = new RpgFighter(6000);
		game.playOneGame(10);
	}
}
