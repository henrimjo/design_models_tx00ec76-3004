package template_method.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import template_method.player.Player;
import template_method.player.Playing;
import template_method.player.PlayingPredicates;

public class RpgFighter extends Game {

	List<Playing> list;
	Long breakTime;
	/**
	 * Creates a game that players of the game can play.
	 * @param milliseconds Maximum time between prints.
	 */
	public RpgFighter(long milliseconds) {
		breakTime = milliseconds;
		list = new ArrayList<>();
	}
	private Optional<Playing> findAvailablePlayer(int playerNum) {
		List<Playing> ListWithOneMember = PlayingPredicates.filterEmployees(list, PlayingPredicates.hasIdAndIsAlive(playerNum));
		list = PlayingPredicates.filterEmployees(list, PlayingPredicates.isAlive());
		Collections.shuffle(list);
		Optional<Playing> player = list.stream().findFirst();
		if(ListWithOneMember.size() != 0) {
			player = ListWithOneMember.stream().findFirst();
		}
		return player;
	}
	@Override
	void initializeGame() {
		if(playersCount < 2) {
			playersCount = 2;
		}
		for (int i = 0; i < playersCount; i++) {
			Playing player = new Player(i);
			player.chooseRole();
			list.add(player);
		}
	}

	@Override
	void makePlay(int player) {
		Optional<Playing> player1;
		Optional<Playing> player2;
		do {
		player1 = findAvailablePlayer(player);
		player2 = findAvailablePlayer(player + 1);
		if(endOfGame()) {
			break;
		}
		} while(player1.equals(player2) || player1.isEmpty() || player2.isEmpty());
		delay(breakTime);
		System.out.println("\n");
		System.out.println("Players still in game: " + list.size());
		System.out.println(player1.get() + " VS "+ player2.get() +" !!");
		System.out.println(player1.get() + " has: "+player1.get().getStats().getHp()+"hp");
		System.out.println(player2.get() + " has: "+player2.get().getStats().getHp()+"hp");
		
		//Players battle
		while(!player2.get().hasLost() || !player1.get().hasLost() &&!endOfGame()) {
			player1.get().attack(player2.get());
			delay(breakTime / 20);
			if(player2.get().hasLost() || player1.get().hasLost()) {
				break;
			}
			player2.get().attack(player1.get());
			delay(breakTime / 20);
		}
		System.out.println("\n");
		if(player1.get().hasLost()) {
			System.out.println(player2.get()+" won the fight!");
		} else {
			System.out.println(player1.get()+" won the fight!");
		}
		delay(breakTime);
	}
	private void delay(long sleepTime) {
		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	boolean endOfGame() {
		if(list.size() < 2) {
			return true;
		}
		return false;
	}
	public boolean endOfGamePublic() {
		return endOfGame();
	}

	@Override
	void printWinner() {
		System.out.println("Winner is " + list.get(0) + "!!");
	}

}
