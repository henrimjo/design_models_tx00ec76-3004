package template_method.player;

import template_method.player.battle.Battle;
import template_method.player.battle.BattleStyleDecider;

public class Player implements Playing {
	private Stats stats;
	private int playersId;
	private boolean dead;
	private Battle battleStyle;
	public Player(int id) {
		playersId = id;
		stats = new Stats((int) (20 * Math.random())+1
				, (int) (5 * Math.random())+1
				, (int) (5 * Math.random())+1
				, (int) (5 * Math.random())+1);
		this.dead = false;
	}
	public final boolean takeDamage(int damage) {
		if(!battleStyle.defend(damage)) {
			stats.setHp(stats.getHp() - damage);
			if(stats.getHp() <= 0) {
				this.dead = true;
			}
			return true;
		}
		return false;
	}
	
	public void chooseRole() {
		PlayerRole role = StatMethods.getRandomRole();
		stats.mergeStats(role.getRoleStats());
		this.battleStyle = BattleStyleDecider.getBattleStyle(this, role);
	}
	
	public void attack(Playing player) {
		battleStyle.attack(player);
	}
	@Override
	public Stats getStats() {
		return stats;
	}
	@Override
	public boolean hasLost() {
		return dead;
	}
	@Override
	public String toString() {
		return "Player" + playersId;
	}
	@Override
	public int getId() {
		return playersId;
	}
}
