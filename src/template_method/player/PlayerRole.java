package template_method.player;

public enum PlayerRole {
	ROGUE(70, 10, 2, 5),WIZARD(60, 5, 5, 10),WARRIOR(100, 3, 10, 3);
	private final Stats roleStats;
	private PlayerRole(int hp, int spd, int str, int wit) {
		roleStats = new Stats(hp, spd, str, wit);
	}
	public Stats getRoleStats() {
		return roleStats;
	}
}
