package template_method.player;

public interface Playing {

	public void attack(Playing player2);
	public void chooseRole();
	public boolean takeDamage(int damage);
	public Stats getStats();
	public int getId();
	public boolean hasLost();
}
