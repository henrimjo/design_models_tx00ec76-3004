package template_method.player;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PlayingPredicates {
	public static Predicate<Playing> isAlive() {
        return p -> !p.hasLost();
    }
	public static Predicate<Playing> isDead() {
        return p -> p.hasLost();
    }
	public static Predicate<Playing> hasIdAndIsAlive(int num) {
        return p -> p.getId() == num && !p.hasLost();
    }
	
 
    public static List<Playing> filterEmployees (List<Playing> employees,
                                                Predicate<Playing> predicate)
    {
        return employees.stream()
                    .filter( predicate )
                    .collect(Collectors.<Playing>toList());
    }
}
