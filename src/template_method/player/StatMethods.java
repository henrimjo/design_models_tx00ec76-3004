package template_method.player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StatMethods {
	public static PlayerRole getRandomRole() {
		//TODO Bug
		List<PlayerRole> list = new ArrayList<>();
		list.add(PlayerRole.ROGUE);
		list.add(PlayerRole.WARRIOR);
		list.add(PlayerRole.WIZARD);
		Collections.shuffle(list);
		return list.get(0);
	}
}
