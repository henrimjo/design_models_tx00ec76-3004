package template_method.player;

public class Stats {
	private int hp, spd, str, wit;
	static final int spdMax = 27, witMax = 24, strMax = 20;
	public Stats(int hp, int spd, int str, int wit) {
		this.hp = hp;
		this.spd = spd;
		this.str = str;
		this.wit = wit;
	}
	public void mergeStats(Stats s) {
		setHp(getHp() + s.getHp());
		setSpd(getSpd() +s.getSpd());
		setStr(getStr() +s.getStr());
		setWit(getWit() +s.getWit());
	}
	public void setIfMax() {
		if(getSpd() > spdMax) {
			setSpd(spdMax);
		}
		if(getWit() > witMax) {
			setWit(witMax);
		}
		if(getStr() > strMax) {
			setStr(strMax);
		}
	}
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getSpd() {
		return spd;
	}
	public void setSpd(int spd) {
		this.spd = spd;
	}
	public int getStr() {
		return str;
	}
	public void setStr(int str) {
		this.str = str;
	}
	public int getWit() {
		return wit;
	}
	public void setWit(int wit) {
		this.wit = wit;
	}
}
