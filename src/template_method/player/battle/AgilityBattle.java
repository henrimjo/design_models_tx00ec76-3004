package template_method.player.battle;

import template_method.player.Playing;

public class AgilityBattle extends AbstractBattle implements Battle{

	public AgilityBattle(Playing player) {
		super(player);
	}

	@Override
	public void attack(Playing player) {
		for(int i=0;i<thisPlayer.getStats().getSpd();i = i+2) {
			int damage = (int) (thisPlayer.getStats().getStr() * Math.random() +1);
			System.out.println(thisPlayer + " chipped with " + damage + " dmg");
			player.takeDamage(damage);
		}
	}

	@Override
	public boolean defend(int attackStrength) {
		if((Math.random() * thisPlayer.getStats().getSpd()) + 1 > attackStrength) {
			System.out.println(thisPlayer + " dodged the attack!");
			return true;
		}
		System.out.println(thisPlayer + " got hit by " + attackStrength);
		return false;
	}

}
