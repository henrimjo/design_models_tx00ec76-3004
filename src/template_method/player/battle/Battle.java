package template_method.player.battle;

import template_method.player.Playing;

public interface Battle {
	public void attack(Playing player);
	public boolean defend(int attackStrength);
}
