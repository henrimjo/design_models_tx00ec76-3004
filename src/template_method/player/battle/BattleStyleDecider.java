package template_method.player.battle;

import template_method.player.PlayerRole;
import template_method.player.Playing;

public class BattleStyleDecider {
	public static Battle getBattleStyle(Playing player, PlayerRole role) {
		switch (role) {
		case WIZARD:
			return new MagicBattle(player);
		case ROGUE:
			return new AgilityBattle(player);
		case WARRIOR:
			return new StrengthBattle(player);
		default:
			return null;
		}
	}
}
