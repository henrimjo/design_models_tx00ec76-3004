package template_method.player.battle;

import template_method.player.Playing;

public class MagicBattle extends AbstractBattle implements Battle {

	public MagicBattle(Playing player) {
		super(player);
	}

	@Override
	public void attack(Playing player) {
		int attackStrength = (int) (Math.random() * thisPlayer.getStats().getWit() + 1);
		System.out.println(thisPlayer + " used MagicBolt worth " + attackStrength + " dmg");
		player.takeDamage(attackStrength);
	}

	@Override
	public boolean defend(int attackStrength) {
		if((Math.random() * thisPlayer.getStats().getStr() +
			Math.random() * thisPlayer.getStats().getSpd()) / 2 + 1 > attackStrength)
		{
			System.out.println(thisPlayer + " blocked using magic!");
			return true;
		}
		System.out.println(thisPlayer + " got hit by " + attackStrength + " dmg");
		return false;
	}

}
