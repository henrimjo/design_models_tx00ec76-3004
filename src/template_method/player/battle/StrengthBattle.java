package template_method.player.battle;

import template_method.player.Playing;

public class StrengthBattle extends AbstractBattle implements Battle{

	public StrengthBattle(Playing player) {
		super(player);
	}

	public void attack(Playing player) {
		int damage = (int) (thisPlayer.getStats().getStr() * Math.random() + 1);
		System.out.println(thisPlayer + " whacked with " + damage + " dmg");
		player.takeDamage(damage);
		
	}

	@Override
	public boolean defend(int attackStrength) {
		if((Math.random() * thisPlayer.getStats().getWit()) + 1 + 
				(Math.random() * thisPlayer.getStats().getSpd()) + 1 > attackStrength) {
			System.out.println(thisPlayer + " managed to stop the attack!");
			return true;
		}
		System.out.println(thisPlayer + " got hit by " + attackStrength + " dmg");
		return false;
	}

}
