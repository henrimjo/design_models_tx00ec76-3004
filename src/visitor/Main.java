package visitor;

import java.util.Scanner;

import visitor.state.Character;
import visitor.state.ICharacter;

public class Main {

	public static void main(String[] args) {
		@SuppressWarnings("resource") // I know it is an resource.
		Scanner scan = new Scanner(System.in);
		StringBuilder builder = new StringBuilder();
		int daysToLive = 0;
		ICharacter character;
		do {
			do {
				//Actions
				System.out.println("\t\t StartMenu");
				System.out.println("======================================================");
				System.out.println("\t Enter The days to live to \n"
									+ "\t Start the game");
				System.out.println("======================================================");
				try {
					daysToLive = Integer.parseInt(scan.nextLine());
				} catch (Exception e) {
					System.out.println("Give a proper number value");
					daysToLive = 0;
				}
				
			} while (daysToLive < 1);
			character = new Character(daysToLive);
			character.live();
			System.out.println("======================================================");
			System.out.println("======================================================");
			System.out.println("---------If you want to restart enter R---------------");
			System.out.println("======================================================");
			System.out.println("======================================================");
			builder.append(scan.nextLine());
			String now = builder.toString().toLowerCase();
			builder.setLength(0);
			builder.append(now);
		} while(builder.indexOf("r") != -1 && builder.length() == 1);
		
	}
}
