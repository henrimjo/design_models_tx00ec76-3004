package visitor.state;

abstract class BaseState implements State {
	protected ICharacter character;
	public BaseState(ICharacter character) {
		this.character = character;
	}
	protected void extra() {
		System.out.println("Gained extra points!");
		character.setExtra(character.getExtra() + 1);
	}
}
