package visitor.state;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import visitor.visiting_protocol.Visited;
import visitor.visiting_protocol.Visitor;
import visitor.visitors.Achievement;
import visitor.visitors.AchievementCreator;

public class Character implements ICharacter,Visited {
	State state;
	Visitor visitor;
	int lifeFeelsGoodPoints;
	int extra;
	int daysToLive;
	Set<Achievement> achievements; 

	public Character(int daysToLive) {
		achievements = new HashSet<>();
		visitor = new AchievementCreator();
		StateStorage storage = StateStorage.getInstance();
		storage.addStates(this);
		storage.getState(this, Full.class)
		.ifPresent(s-> state = s);
		this.daysToLive = daysToLive;
		extra = 0;
		lifeFeelsGoodPoints = 0;
	}
	//====================================//
	//=========GETTERS=&&=SETTERS=========//
	//====================================//
	public int getExtra() {
		return extra;
	}
	public void setExtra(int extra) {
		this.extra = extra;
	}
	public void addAchievement(Achievement achievement) {
		if(!achievements.contains(achievement))
			achievements.add(achievement);
	}

	public void setState(State state) {
		this.state = state;
	}

	public State getState() {
		return state;
	}
	public int getPoints() {
		return lifeFeelsGoodPoints;
	}

	public void setPoints(int lifeFeelsGoodPoints) {
		this.lifeFeelsGoodPoints = lifeFeelsGoodPoints;
	}
	public void setDaysToLive(int days) {
		daysToLive = days;
	}
	//====================================//
	//====================================//
	
	@SuppressWarnings("resource") // I know it is unused.
	Scanner scan = new Scanner(System.in);
	String consoleOutput = "1";
	StringBuilder builder = new StringBuilder();
	public void live() {
		do {
			//Actions
			System.out.println("\t\t Actions");
			System.out.println("======================================================");
			System.out.println("\t Enter 1 to Eat");
			System.out.println("\t Enter 2 to Sleep");
			System.out.println("\t Enter 0 to skip (get 1 bonus action by skiping ones)");
			System.out.println("\t Enter - to Stop the program");
			System.out.println("======================================================");
			int actions = 3;
			boolean extraTurn = true;
			for (;actions >= 0 ; actions--) {
				builder.append(scan.nextLine());
				if(builder.indexOf("1") != -1) {
					state.eat();
				} else if(builder.indexOf("2") != -1) {
					System.out.println("Slept");
					state.sleep();
				} else if(builder.indexOf("0") != -1) {
					System.out.println("did nothing");
					state.doNothing();
					if(extraTurn) {
						extraTurn = false;
						actions++;
					}
				} else if(builder.indexOf("-") != -1) {
					actions = 0;
					System.out.println("Bye bye!");
					System.exit(1);
				}
				builder.setLength(0);
				System.out.println("Action recorded, actions left: " + actions);
			}
			System.out.println("Character gave his life " + getPoints() +
					" points today. \" I feel " + getState().getClass().getSimpleName()+ "\"");
			System.out.println(daysToLive + " days left.");
			accept(visitor);
			daysToLive--;
		} while (daysToLive > 0);
		System.out.println("Gained achievements");
		System.out.println(combineAchievements().toString());
		if(daysToLive < 0) {
			System.out.println("Is this heaven??");
		}
		System.out.println("");
	}
	private StringBuilder combineAchievements() {
		StringBuilder stringBuilder = new StringBuilder();
		for (Achievement achievement : achievements) {
			stringBuilder.append(achievement.getName());
			stringBuilder.append(" ");
		}
		return stringBuilder;
	}

	//Accepts many visitors at ones!
	@Override
	public void accept(Visitor visitor) {
		visitor.visitNormally(this);
		visitor.visitGive(this);
		visitor.visitEnd(this);
	}
}
