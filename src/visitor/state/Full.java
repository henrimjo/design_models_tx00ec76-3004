package visitor.state;

public class Full extends BaseState implements State {
	public Full(ICharacter character) {
		super(character);
	}

	int notEaten;

	@Override
	public void initialState() {
		notEaten = 3;	
	}
	@Override
	public void eat() {
		changeState();
		notEaten = 3;
		character.setPoints(character.getPoints() - 2 + character.getExtra());
	}

	@Override
	public void sleep() {
		changeState();
		notEaten--;
		character.setPoints(character.getPoints() + 5 + character.getExtra());
	}

	@Override
	public void doNothing() {
		changeState();
		notEaten--;
		character.setPoints(character.getPoints() + 1 + character.getExtra());
		if (Math.random() < 0.05) {
			extra();
		}
	}

	@Override
	public ICharacter getCharacter() {
		return character;
	}

	@Override
	public void changeState() {
		if (notEaten <= 0) {
			StateStorage.getInstance().getState(character, Peckish.class).ifPresent(s -> character.setState(s));
		}

	}

}
