package visitor.state;

public class Hungry extends BaseState implements State {
	public Hungry(ICharacter character) {
		super(character);
	}

	int eaten;
	public void initialState() {
		eaten = 0;
	}

	@Override
	public void eat() {
		changeState();
		eaten++;
		character.setPoints(character.getPoints() + 5 - Math.abs(eaten) + character.getExtra());
	}

	// Because of hungry you sleep real bad.
	private int timesSleeped = 9;

	@Override
	public void sleep() {
		changeState();
		timesSleeped++;
		eaten--;
		character.setPoints(character.getPoints() + 10 - timesSleeped - Math.abs(eaten) + character.getExtra());
	}

	@Override
	public void doNothing() {
		changeState();
		eaten--;
		character.setPoints(character.getPoints() - 3 - Math.abs(eaten) + character.getExtra());
		if (Math.random() < 0.10) {
			extra();
		}
	}

	@Override
	public ICharacter getCharacter() {
		return character;
	}

	@Override
	public void changeState() {
		if (eaten > 3) {
			if (Math.random() < 0.05) {
				extra();
			}
			StateStorage.getInstance().getState(character, Peckish.class).ifPresent(s -> character.setState(s));
		}

	}

}
