package visitor.state;

public interface ICharacter {
	public void live();
	public State getState();
	public void setState(State state);
	public void setPoints(int Points);
	public int getPoints();
	public int getExtra();
	public void setExtra(int extra);
}
