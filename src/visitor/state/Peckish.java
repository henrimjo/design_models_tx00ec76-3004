package visitor.state;

public class Peckish extends BaseState implements State {

	public Peckish(ICharacter character) {
		super(character);
	}

	int eaten;

	public void initialState() {
		eaten = 1;
	}

	@Override
	public void eat() {
		changeState();
		eaten++;
		character.setPoints(character.getPoints() + 3 + character.getExtra());
	}

	@Override
	public void sleep() {
		changeState();
		eaten--;
		character.setPoints(character.getPoints() + 1 + character.getExtra());
	}

	@Override
	public void doNothing() {
		changeState();
		eaten--;
		character.setPoints(character.getPoints() - 2 - character.getExtra());
		if (Math.random() < 0.07) {
			extra();
		}
	}

	@Override
	public ICharacter getCharacter() {
		return character;
	}

	@Override
	public void changeState() {
		if (eaten > 3) {
			if (Math.random() < 0.05) {
				extra();
			}
			StateStorage.getInstance().getState(character, Full.class).ifPresent(s -> character.setState(s));
		}
		if (eaten < 0) {
			StateStorage.getInstance().getState(character, Hungry.class).ifPresent(s -> character.setState(s));
		}

	}
}
