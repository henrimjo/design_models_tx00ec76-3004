package visitor.state;

/**
 * 
 * @author Henri Johansson
 * Returns points
 */
interface State {
	public void initialState();
	public void eat();
	public void sleep();
	public void doNothing();
	public void changeState();
	public ICharacter getCharacter();
}
