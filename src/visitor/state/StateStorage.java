package visitor.state;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class StateStorage {
	private static StateStorage instance;
	private Set<State> stateSet = new HashSet<State>();
	private StateStorage() {
	}
	public static StateStorage getInstance() {
		if(instance == null) {
			instance = new StateStorage();
		}
		return instance;
	}
	public void addStates(ICharacter character) {
		stateSet.add(new Full(character));
		stateSet.add(new Peckish(character));
		stateSet.add(new Hungry(character));
	}
	public Optional<State> getState(ICharacter character, Class<? extends State> clazz) {
		for (State state : stateSet) {
			if(state.getClass() == clazz && state.getCharacter() == character) {
				state.initialState();
				return Optional.of(state);
			}
		}
		return Optional.empty();
	}
}
