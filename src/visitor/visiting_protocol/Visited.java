package visitor.visiting_protocol;

public interface Visited {
	public void accept(Visitor visitor);
}
