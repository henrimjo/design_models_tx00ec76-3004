package visitor.visiting_protocol;

public interface Visitor {
	public void visitNormally(Visited visited) throws IllegalArgumentException;
	public void visitGive(Visited visited) throws IllegalArgumentException;
	public void visitEnd(Visited visited) throws IllegalArgumentException;
}
