package visitor.visitors;

public class Achievement {
	String name;
	int rewardPoints;
	
	public Achievement(String name, int rewardPoints) {
		super();
		this.name = name;
		this.rewardPoints = rewardPoints;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRewardPoints() {
		return rewardPoints;
	}

	public void setRewardPoints(int rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
}
