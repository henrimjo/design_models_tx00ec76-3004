package visitor.visitors;

import visitor.state.Character;
import visitor.state.Full;
import visitor.visiting_protocol.Visited;
import visitor.visiting_protocol.Visitor;

public class AchievementCreator implements Visitor{

	/**
	 * Normal visiting gives some bonus-points
	 */
	@Override
	public void visitNormally(Visited visited) throws IllegalArgumentException {
		Character character = ifInstanceCharacter(visited);
		if(character.getExtra() > 7) {
			character.setPoints(character.getPoints() + 10);
		}
		//Some bonus-points
		character.setPoints(character.getPoints() + 10);
	}

	/**
	 * Give achievements if there is a special case for it.
	 */
	@Override
	public void visitGive(Visited visited) throws IllegalArgumentException {
		Character character = ifInstanceCharacter(visited);
		if(character.getPoints() == 69) {
			character.addAchievement(new Achievement("Funny", 69));
		}
		if(character.getPoints() < -100) {
			character.addAchievement(new Achievement("Dead?", 10));
		}
		if (character.getPoints() > 100 && character.getState() instanceof Full) {
			character.addAchievement(new Achievement("Doing way too good :P", -10));
		}
		
		
	}

	/**
	 * Kill the character if character is suffering too much.
	 */
	@Override
	public void visitEnd(Visited visited) throws IllegalArgumentException {
		Character character = ifInstanceCharacter(visited);
		if(character.getPoints() < -100) {
			character.setDaysToLive(0);
		}
	}
	private Character ifInstanceCharacter(Visited visited) throws IllegalArgumentException {
		if(visited instanceof Character) {
			 return ((Character) visited);
		} else {
			throw new IllegalArgumentException(visited.getClass() + " is not accepted argument");
		}
	}

}
